#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "s1ch.h"

int get_score(char * input, int size) 
{
        int score = 0; /* letter freq */

        for (int i = 0; i < size; i++) {
                /* if input[i] is a char increment */
                if (((input[i] > 'A') && (input[i] < 'Z'))
                || ((input[i] > 'a') && (input[i] < 'z'))
                || (input[i] == 0x20)) {
                        score++;
                }
        }

        return score;
}

char *s1ch3_xor(char *input_1, char input_2, int size)
{
        char *result = NULL;
        result = calloc(size + 1, sizeof(char));
        
        for (int i = 0; i < size; i++) {
               result[i] = input_1[i] ^ input_2; 
        }

        return  result;
}

int main_s1ch3() 
{
        char input[] = "1b37373331363f78151b7f2b783431333d78397828372d363c78373e783a393b3736",
             *b16_string = NULL, /* input converted to hex */
             *result = NULL; /* results of hex values xord */
        int score[255], /* 255 ascii characters*/
            len = 0,
            best_fit = 0; /* char with highest score */ 
        /* get strlen of hex string */
        len = ascii_b16_size(strlen(input));
        /* converst string to hex */
        b16_string = hstring_convert(input);
        /* xor against chars */
        for (int i = 0; i < 255; i++) {
                result = s1ch3_xor(b16_string, i, len);
                score[i] = get_score(result, len);
                if (i > 0)
                      best_fit = (score[best_fit] > score[i]) ? best_fit : i;
                if (result)
                        free(result);
        }

        /* display best results output */
        result = s1ch3_xor(b16_string, best_fit, len);
        printf("Char: %c\nResult: %s\n", best_fit, result);

        /* clean up */
        if (b16_string)
                free(b16_string);
        if (result)
                free(result);

        return 1;
}
