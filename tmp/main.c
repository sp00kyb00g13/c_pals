#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "s1ch.h"

#define b16_lower_mask 0b00001111

char *b16_string_converter(char *b16_input, int size)
{
        char *ascii_string = NULL; /* ascii converted string */
        char *ascii_string_start = NULL; /* reference to start of string */
        
        ascii_string = calloc(b16_ascii_size(size) + 1, sizeof(char));
        ascii_string_start = ascii_string;
        
        for (int i = 0; i < size; i++) {
                /* convert upper 4 bits then repeat for lower 4 bits */
                ascii_string[0] = hex_to_ascii((b16_input[i] >> 4) & b16_lower_mask);
                ascii_string++;
                ascii_string[0] = hex_to_ascii(b16_input[i] & b16_lower_mask);
                ascii_string++;
        }

        /* add null terminator */
        ascii_string[strlen(ascii_string)] = '\0';
        return ascii_string_start;
}
char *hstring_convert(char *hinput)
{
        char *b16_string, /* index of current position in the conversion */
             *b16_start; /* index of start */
        int size = 0;
        
        /* each char is 8 bits, hex is only 4 bits, thus we need half
         * the strlen to store the whole string in hex format
         */
        size = strlen(hinput);
        b16_string = calloc(ascii_b16_size(size) + 1, sizeof(char));
        b16_start = b16_string;

        for (int i = 0; i < size; i = i + 2) {
                /* get substring then convert to base64, put first char
                 * in upper 4bits, second in lower
                 */
                b16_string[0] = (ascii_to_hex(hinput[i + 1]))
                                | (ascii_to_hex(hinput[i]) << 4);
                b16_string++;
        }
        /* add null terminator */
        b16_start[ascii_b16_size(size)] = '\0';
        return b16_start;
}

int ascii_to_hex(char input)
{
        return (input >= 'A') ? input - 0x57 : input - 0x30;
}

char hex_to_ascii(char input)
{
        return (input >= 0xA) ? input + 0x57: input + 0x30;
}

int main() 
{
        //main_s1ch1();
        //main_s1ch2();
        //main_s1ch3();
        main_s1ch4();
        return 0;
}

