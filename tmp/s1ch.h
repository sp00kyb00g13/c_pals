#ifndef s1ch_h
#define s1ch_h

#define ascii_b16_size(size) ((size + 1) / 2)
#define b16_ascii_size(size) (size * 2)

/* set 1 challenge 1 */
char *to_64(char *b16_string, int size);
int main_s1ch1();

/* set 1 challenge 2 */
int main_s1ch2();
char *s1ch2_xor(char *input_1, char *input_2, int len);

/* set 1 challenge 3 */
int main_s1ch3();
char *s1ch3_xor(char *input_1, char input_2, int size);
int get_score(char * input, int size);

/* set 1 challenge 4 */
int main_s1ch4();

char hex_to_ascii(char input);
int ascii_to_hex(char input);
char * hstring_convert(char *);
char *b16_string_converter(char *b16_input, int size);

#endif /* s1ch_h */
