#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "s1ch.h"

char *s1ch2_xor(char *input_1, char *input_2, int len)
{
        char *result = NULL; /* xor result */

        /* both strings should have same length 
        * and we are converting len from acii to hex
        */
        result = calloc(len, sizeof(char));

        for (int i = 0; i < len; i++) {
                result[i] = input_1[i] ^ input_2[i];
        }

        return result;
}

int main_s1ch2() 
{
        char input_1[] = "1c0111001f010100061a024b53535009181c",
             input_2[] = "686974207468652062756c6c277320657965",
             *b16_string1 = NULL, /* string to hold binary values of input1 */
             *b16_string2 = NULL, /* string to hold binary values of input2 */
             *result_b16 = NULL, /* xor result */
             *result_ascii = NULL; /* xor result in ascii */

        b16_string1 = hstring_convert(input_1);
        b16_string2 = hstring_convert(input_2);
        printf("%s\n%s\n", b16_string1, b16_string2);

        result_b16 = s1ch2_xor(b16_string1, b16_string2, ascii_b16_size(strlen(input_1)));
        result_ascii = b16_string_converter(result_b16, ascii_b16_size(strlen(input_1)));

        printf("%s\n", result_ascii);
        
        /* cleanup */
        if (b16_string1)
                free(b16_string1);
        if (b16_string2)
                free(b16_string2);
        if (result_b16)
                free(result_b16);
        return 1;
}
