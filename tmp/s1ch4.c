#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "s1ch.h"

int main_s1ch4()
{
        char buffer[512], /* buffer for reading input */
             *b16_string = NULL;

        FILE *input = NULL, /* input txt file */
             *output = NULL;

        input = fopen("4.txt", "r");
        if (input == NULL) {
                perror("fopen");
                return 0;
        }

        output = fopen("output.txt", "w");
        if (input == NULL) {
                perror("fopen");
                return 0;
        }

        while(fgets(buffer, 512, input)) {
               b16_string = hstring_convert(buffer);
               fprintf(output, "%s\n", b16_string);
               free(b16_string);
        }

        if (input)
                fclose(input);
        if (output)
                fclose(output);
        return 1;
}
