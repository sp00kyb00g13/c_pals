#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "s1ch.h"

static const int i64_upper_mask = 0b111111000000;
static const int i64_lower_mask = 0b000000000000000000111111;

static char encoding_table[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
                                'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                                'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                                'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n',
                                'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
                                'w', 'x', 'y', 'z', '0', '1', '2', '3',
                                '4', '5', '6', '7', '8', '9', '+', '/'};

char *to_64(char *b16_string, int size)
{
        char *b64_string, /* index we are at in the conversion */
             *b64_start; /* starting addr of result */
        int index = 0, /* j will hold loop count for b64_String */
            length = 0; /* length of the input string*/

        /* char is 8 bits b64 is 6 bits 
         * let x = strlen(b16_string)
         * let z = strlen(b64_string)
         * let y = strlen(input)
         * x = 1/2y (4 bits per hex, 8 bits per char)
         * z = 3/2x (6 bits per b64, 4 bits per hex)
         * z = 3/4y (combining previous equations
         */
        b64_string = calloc((size + 2) * 2 / 3, sizeof(char));
        b64_start = b64_string;
        length = strlen(b16_string);

        for (int i = 0; i < length; i = i + 3) {
                index = (b16_string[i] << 16)
                        | (b16_string[i+1] << 8)
                        | b16_string[i+2];
                /* get 6 bits at a time */
                b64_string[0] = encoding_table[index >> 18 & i64_lower_mask];
                b64_string++;
                b64_string[0] = encoding_table[(index >> 12) & i64_lower_mask];
                b64_string++;
                b64_string[0] = encoding_table[(index >> 6) & i64_lower_mask];
                b64_string++;
                b64_string[0] = encoding_table[(index) & i64_lower_mask];
                b64_string++;
        }
        b64_start[strlen(b64_start)-1] = '\0';

        return b64_start;
}

int main_s1ch1() {
        char *b16_string, /* hex string after conversion */
             *b64_string, /* base 64 string after conversion */
             hex_string[] = "49276d206b696c6c696e6720796f757220627261696e206c696b65206120706f69736f6e6f7573206d757368726f6f6d";
        int size = strlen(hex_string) + 1;

        b16_string = hstring_convert(hex_string);
        printf("%s\n", b16_string);
        b64_string = to_64(b16_string, size);
        printf("%s\n", b64_string);

        free(b16_string);
        free(b64_string);
        return 1;
}
